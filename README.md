prožektors
==========

prožektors - data mining to stop sex trafficking and pimping.

"prožektors" is the Latvian word for "searchlight," the working title of this project.  Latvian was chosen
because of the song "Riga Girls" by The Weepies - a personal favorite song of mine, the subject of which is
porn spam titled "Riga Girls Go Like This."

In a nutshell
-------------

1. Scrape red-light areas of the internet and try to correlate workers across many ads and sites, building
   a list of related ads and a trove of data on when, where ads were placed, text of ads and photos that
   we hope are related to a single worker.  (this is document classification)
1. Scrape red-light discussion forums and correlate threads and/or posts to workers identified above.
1. Classify the content of the correlated threads and/or posts to determine, much like an email spam filter
   works, the likelihood that there is trafficking or coercion involved. (again, this is document classification)
1. Spit out a report on the most likely victims of sexual coercion being advertised online for a given geographical area.

These are not easy problems to tackle.  There are two types of document classification, plus correlation between workers
and johns' posted content about them.
